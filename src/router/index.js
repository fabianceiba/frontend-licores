import gql from "graphql-tag";
import { createRouter, createWebHistory } from "vue-router";
import { ApolloClient, createHttpLink, InMemoryCache } from '@apollo/client/core'
import LogIn from '../components/LogIn.vue'
import SignUp from '../components/SignUp.vue'
import Catalog from '../components/Catalog.vue'
import Compras from '../components/Compras.vue'
import Detalles from '../components/Detalles.vue'

const routes = [{
  path: '/user/logIn',
  name: "logIn",
  component: LogIn,
  meta: { requiresAuth: false }
},
{
  path: '/user/signUp',
  name: "signUp",
  component: SignUp,
  meta: { requiresAuth: false }
},

{
  path: '/catalog',
  name: "Catalog",
  component: Catalog,
  meta: { requiresAuth: false }
},
{
  path: '/catalogo/detalles',
  name: "Detalles",
  component: Detalles,
  meta: { requiresAuth: false }
},
{
  path: '/user/compras',
  name: "compras",
  component: Compras,
  meta: { requiresAuth: false }
},

];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

const apolloClient = new ApolloClient({
  link: createHttpLink({ uri: 'https://api-gateway-licores-g3p53.herokuapp.com' }),
  cache: new InMemoryCache()
})

async function isAuth() {
  if (localStorage.getItem("token_access") === null ||
    localStorage.getItem("token_refresh") === null) {
    return false;
  }
  try {
    var result = await apolloClient.mutate({
      mutation: gql`
        mutation ($refresh: String!) {
          refreshToken(refresh: $refresh) {
            access
          }
        }
        `,
      variables: {
        refresh: localStorage.getItem("token_refresh"),
      },
    })
    localStorage.setItem("token_access", result.data.refreshToken.access);
    return true;
  } catch {
    localStorage.clear();
    alert("Su sesión expiró, por favor vuelva a iniciar sesión");
    return false;
  }
}
router.beforeEach(async (to, from) => {
  var is_auth = await isAuth();
  if (is_auth == to.meta.requiresAuth) return true
  if (is_auth) return { name: "home" };
  return { name: "logIn" };
})
export default router;
